package org.apache.mytest;

import org.apache.jmeter.processor.PostProcessor;
import org.apache.jmeter.testelement.AbstractScopedTestElement;
import org.apache.jmeter.testelement.ThreadListener;
import org.apache.jmeter.threads.JMeterContext;
import org.apache.jmeter.threads.JMeterVariables;

import java.io.Serializable;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/3 15:11
 */
public class MyEncDecProcessor extends AbstractScopedTestElement implements Serializable, PostProcessor, ThreadListener {

	@Override
	public void process() {
		JMeterContext context = getThreadContext();
		JMeterVariables vars = context.getVariables();
		System.out.println("MyEncDecProcessor | process | " + vars.get("data"));
	}

	@Override
	public void threadStarted() {
	}

	@Override
	public void threadFinished() {
	}
}
