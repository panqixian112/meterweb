package cn.fetosoft.woodpecker.controller;

import cn.fetosoft.woodpecker.enums.MessageType;
import com.alibaba.fastjson.JSON;
import lombok.Getter;
import lombok.Setter;

/**
 * Socket消息
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/6 20:38
 */
@Setter
@Getter
public class SocketMessage<T> {

    /**
     * 1-conn message
     * 2-testComplete
     */
    private int type;

    private String message;

    private T data;

    public SocketMessage(MessageType type, String message){
        this.type = type.getName();
        this.message = message;
    }

    @Override
    public String toString(){
        return JSON.toJSONString(this);
    }
}
