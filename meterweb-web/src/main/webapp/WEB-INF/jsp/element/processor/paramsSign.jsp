<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">HTTP参数签名处理器</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'paramsSign_form_', function(eid, status) {
                            if(status===1){
                            menuTree.updateNodeText($('#paramsSign_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="paramsSign_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="name" id="paramsSign_form_name_${eid}" style="width:90%" data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:90%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p style="padding-left: 150px;">参与加签的参数自动按首字母的顺序排列</p>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="signName" style="width:90%" data-options="label:'签名变量名称：',labelWidth:150,required:true">
                    </div>
                    <div class="form_item_height">
                        <span style="margin-right: 90px;">签名算法：</span>
                        <input class="easyui-radiobutton" name="signType" value="md5" data-options="label:'MD5',labelWidth:80,labelPosition:'after',checked:true">
                        <input class="easyui-radiobutton" name="signType" value="hmac" data-options="label:'HmacSHA1',labelWidth:80,labelPosition:'after'">
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="signKey" style="width:90%" data-options="label:'Hmac加签密钥：',labelWidth:150">
                    </div>
                    <p style="padding-left: 150px;">多个“加签参数名”、“path表达式”使用英文“;”隔开</p>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="paramName" style="width:90%" data-options="label:'参与加签参数名称：',labelWidth:150,required:true">
                    </div>
                    <p style="padding-left: 150px;">当POST内容格式为JSON时，提取参数的JSON PATH表达式</p>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="jsonPath" style="width:90%" data-options="label:'JSON Path表达式：',labelWidth:150">
                    </div>
                    <div>
                        <p style="padding-left: 150px;">参数拼接符默认使用“&”</p>
                        <input class="easyui-textbox" name="signJoiner" style="width:90%" data-options="label:'加签参数拼接符：',labelWidth:150">
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}', 'paramsSign_form_', function(eid, status) {
                    if(status===1){
                    menuTree.updateNodeText($('#paramsSign_form_name_' + eid).val());
                    }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>