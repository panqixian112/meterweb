<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/static/jsp/taglibs.jsp"%>
<div class="pt-3 pl-3" style="width:1000px;">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">HTTP请求</h3>
            <ul class="nav nav-pills card-header-pills">
                <li class="nav-item ml-auto">
                    <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','httpSampler_form_',function(eid,status) {
                            if(status===1){
                            menuTree.updateNodeText($('#httpSampler_form_name_' + eid).val());
                            }
                            })" class="btn btn-bitbucket">
                        <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                            <path stroke="none" d="M0 0h24v24H0z"></path>
                            <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                            <circle cx="12" cy="14" r="2"></circle>
                            <polyline points="14 4 14 8 8 8 8 4"></polyline>
                        </svg>
                        保存
                    </a>
                </li>
            </ul>
        </div>
        <form id="httpSampler_form_${eid}" method="post">
            <ul class="list-group card-list-group">
                <li class="list-group-item py-4">
                    <input type="hidden" name="id">
                    <input type="hidden" name="parentId">
                    <input type="hidden" name="planId">
                    <input type="hidden" name="category">
                    <div class="form_item_height">
                        <input class="easyui-textbox" id="httpSampler_form_name_${eid}" name="name" style="width:100%"
                               data-options="label:'名称：',labelWidth:150,required:true">
                    </div>
                    <div>
                        <input class="easyui-textbox" name="dest" style="width:100%" data-options="label:'描述:',labelWidth:150">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>为空则使用HTTP默认配置：</p>
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-4">
                                <select class="easyui-combobox" name="protocol" style="width:100%;"
                                        data-options="label:'HTTP协议：',labelWidth:150,
                                                            valueField: 'value',
                                                            panelHeight: 100,
                                                            textField: 'label',
                                                            data: [{
                                                                label: 'HTTPS',
                                                                value: 'https'
                                                            },{
                                                                label: 'HTTP',
                                                                value: 'http'
                                                            }]">
                                </select>
                            </div>
                            <div class="col-4">
                                <input class="easyui-textbox" name="domain" style="width:100%" data-options="label:'域名或IP：',labelWidth:100">
                            </div>
                            <div class="col-4">
                                <input class="easyui-numberbox" name="port" style="width:100%" data-options="label:'端口号：',labelWidth:100">
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="row">
                            <div class="col-4">
                                <input class="easyui-numberbox" name="connectTimeout" style="width:100%" data-options="label:'连接超时（毫秒）：',labelWidth:150">
                            </div>
                            <div class="col-4">
                                <input class="easyui-numberbox" name="responseTimeout" style="width:100%" data-options="label:'响应超时（毫秒）：',labelWidth:150">
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="form_item_height">
                        <div class="row">
                            <div class="col-4">
                                <select class="easyui-combobox" name="method" style="width:100%;" data-options="label:'HTTP请求：',labelWidth:150">
                                    <option value="POST">POST</option>
                                    <option value="GET">GET</option>
                                    <option value="PUT">PUT</option>
                                    <option value="DELETE">DELETE</option>
                                    <option value="HEAD">HEAD</option>
                                    <option value="OPTIONS">OPTIONS</option>
                                    <option value="TRACE">TRACE</option>
                                    <option value="PATCH">PATCH</option>
                                </select>
                            </div>
                            <div class="col-4">
                                <input class="easyui-textbox" name="contentEncode" style="width:100%" data-options="label:'内容编码：',labelWidth:100">
                            </div>
                            <div class="col-4"></div>
                        </div>
                    </div>
                    <div class="form_item_height">
                        <input class="easyui-textbox" name="path" style="width:100%" data-options="label:'请求路径：',labelWidth:150">
                    </div>
                    <div style="padding-left: 150px;">
                        <input class="easyui-checkbox" name="autoRedirects" value="true" data-options="label:'自动重定向：',labelPosition:'after',labelWidth:140">
                        <input class="easyui-checkbox" name="followRedirects" value="true" data-options="label:'跟随重定向：',labelPosition:'after',labelWidth:140">
                        <input class="easyui-checkbox" name="useKeepalive" value="true" data-options="label:'使用KeepAlive：',labelPosition:'after',labelWidth:140">
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <div class="row">
                        <div class="col-2">内容类型：</div>
                        <div class="col-10">
                            <input class="easyui-radiobutton" name="postBodyRaw" value="json"
                                   data-options="label:'application/json',labelPosition:'after',labelWidth:160,checked:true,
                                                onChange:function(checked){
                                                    if(checked){
                                                        wpElement.httpSamplerChange('${eid}','json');
                                                    }
                                                }">
                            <input class="easyui-radiobutton" name="postBodyRaw" value="form"
                                   data-options="label:'application/x-www-form-urlencoded',labelPosition:'after',labelWidth:260,
                                                onChange:function(checked){
                                                    if(checked){
                                                        wpElement.httpSamplerChange('${eid}','form');
                                                    }
                                                }">
                            <input class="easyui-radiobutton" name="postBodyRaw" value="multipart"
                                   data-options="label:'multipart/form-data',labelPosition:'after',labelWidth:160,
                                                onChange:function(checked){
                                                    if(checked){
                                                        wpElement.httpSamplerChange('${eid}','multipart');
                                                    }
                                                }">
                        </div>
                    </div>
                </li>
                <li class="list-group-item py-4">
                    <p>
                        请求参数格式：<br/>
                        1、“application/json”参数格式为JSON格式数据：{"param1":"value1","param2":"value2","param2":"value2"}<br/>
                        2、“application/x-www-form-urlencoded”模拟表单方式提交<br/>
                        3、“multipart/form-data”模拟表单方式提交且支持上传附件，暂未实现
                    </p>
                    <div id="httpSampler_body_${eid}">
                        <input id="httpSampler_form_arguments_${eid}" class="easyui-textbox" name="arguments" style="width:90%"
                               data-options="multiline:true,height:200">
                    </div>
                    <div id="httpSampler_argument_${eid}" class="card" style="display: none">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-5">参数名称</div>
                                <div class="col-5">参数值</div>
                                <div class="col-2"><a href="javascript:void(0)" onclick="wpElement.httpParameterAdd($('#httpSampler_grid_${eid}'),'','')" class="btn btn-outline-primary">新增</a></div>
                            </div>
                            <div id="httpSampler_grid_${eid}">
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
        <div class="card-footer">
            <a href="javascript:void(0)" onclick="testElement.saveElement('${eid}','httpSampler_form_',function(eid,status) {
                        if(status===1){
                            menuTree.updateNodeText($('#httpSampler_form_name_' + eid).val());
                        }
                    })" class="btn btn-bitbucket">
                <svg xmlns="http://www.w3.org/2000/svg" class="icon icon-md" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <path stroke="none" d="M0 0h24v24H0z"></path>
                    <path d="M6 4h10l4 4v10a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2v-12a2 2 0 0 1 2 -2"></path>
                    <circle cx="12" cy="14" r="2"></circle>
                    <polyline points="14 4 14 8 8 8 8 4"></polyline>
                </svg>
                保存
            </a>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $.get('${ctx}/auth/element/getDetail?id=${eid}', function (data) {
            if(data.status===1){
                let names = data.obj.names;
                let values = data.obj.values;
                if(names && names.length>0){
                    let container = $('#httpSampler_grid_${eid}');
                    for(let i=0;i<names.length; i++){
                        wpElement.httpParameterAdd(container,names[i],values[i]);
                    }
                }
            }
        });
    });
</script>