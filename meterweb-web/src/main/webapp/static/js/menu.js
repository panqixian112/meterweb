let menuItems = {
    copy: {
        text: '复制',
        iconCls: 'icon-copy',
        onclick: function () {
            menuTree.copy();
        }
    },
    paste: {
        text: '粘贴',
        iconCls: 'icon-paste',
        onclick: function () {
            menuTree.paste();
        }
    },
    delete: {
        text: '删除',
        iconCls: 'icon-delete',
        onclick: function () {
            menuTree.delElement(function(node) {
                testPlanTab.closeCurrentTab();
            });
        }
    },
    helper: {
        text: '帮助',
        iconCls: 'icon-help1',
        onclick: function () {

        }
    },
    separator: {
        separator: true
    },
    enabled: {
        text: '启用',
        onclick: function () {
            menuTree.enabled(true);
        }
    },
    disabled: {
        text: '禁用',
        onclick: function () {
            menuTree.enabled(false);
        }
    },
    modifySort: {
        text: '编辑排序',
        onclick: function () {
            menuTree.modifySort();
        }
    },
    moveUp: {
        text: '上移',
        iconCls: 'icon-up',
        onclick: function () {
            menuTree.upOrDown('up');
        }
    },
    moveDown: {
        text: '下移',
        iconCls: 'icon-down',
        onclick: function () {
            menuTree.upOrDown('down');
        }
    },
    threadGroup: {
        text: '线程组',
        iconCls: 'icon-thread',
        onclick: function () {
            menuTree.addElement('threadGroup');
        }
    },
    csvDataSet: {
        text: 'CSV数据文件设置',
        onclick: function () {
            menuTree.addElement('csvDataSet');
        }
    },
    httpConfig: {
        text: 'HTTP请求默认值',
        onclick: function () {
            menuTree.addElement('httpConfig');
        }
    },
    httpHeader: {
        text: 'HTTP消息头管理器',
        onclick: function () {
            menuTree.addElement('httpHeader');
        }
    },
    dataSourceConfig: {
        text: 'JDBC数据源配置',
        onclick: function () {
            menuTree.addElement('dataSourceConfig');
        }
    },
    idGenerator: {
        text: '随机ID生成器',
        onclick: function () {
            menuTree.addElement('idGenerator');
        }
    },
    userArgument: {
        text: '用户定义的变量',
        onclick: function () {
            menuTree.addElement('userArgument');
        }
    },
    httpSampler: {
        text: 'HTTP请求',
        onclick: function () {
            menuTree.addElement('httpSampler');
        }
    },
    debugSampler: {
        text: 'Debug取样器',
        onclick: function () {
            menuTree.addElement('debugSampler');
        }
    },
    jdbcRequest: {
        text: 'JDBC Request',
        onclick: function () {
            menuTree.addElement('jdbcRequest');
        }
    },
    beanShellSampler: {
        text: 'BeanShell取样器',
        onclick: function () {
            menuTree.addElement('beanShellSampler');
        }
    },
    paramsSign:{
        text: 'HTTP参数签名',
        onclick: function () {
            menuTree.addElement('paramsSign');
        }
    },
    jsonExtract: {
        text: 'JSON提取器',
        onclick: function () {
            menuTree.addElement('jsonExtract');
        }
    },
    jdbcPre:{
        text: 'JDBC前置处理器',
        onclick: function () {
            menuTree.addElement('jdbcPre');
        }
    },
    jdbcPost:{
        text: 'JDBC后置处理器',
        onclick: function () {
            menuTree.addElement('jdbcPost');
        }
    },
    beanShellPre:{
        text: 'BeanShell前置处理器',
        onclick: function () {
            menuTree.addElement('beanShellPre');
        }
    },
    beanShellPost:{
        text: 'BeanShell后置处理器',
        onclick: function () {
            menuTree.addElement('beanShellPost');
        }
    },
    resultAction:{
        text: '结果状态处理器',
        onclick: function () {
            menuTree.addElement('resultAction');
        }
    },
    regexExtractor: {
        text: '正则表达式提取器',
        onclick: function () {
            menuTree.addElement('regexExtractor');
        }
    },
    aesSecret: {
        text: 'AES加解密处理器',
        onclick: function () {
            menuTree.addElement('aesSecret');
        }
    },
    debugPost: {
        text: '调试后置处理器',
        onclick: function () {
            menuTree.addElement('debugPost');
        }
    },
    criticalSection: {
        text: '临界部分控制器',
        onclick: function () {
            menuTree.addElement('criticalSection');
        }
    },
    ifController: {
        text: 'IF控制器',
        onclick: function () {
            menuTree.addElement('ifController');
        }
    },
    moduleController: {
        text: '模块控制器',
        onclick: function () {
            menuTree.addElement('moduleController');
        }
    },
    loopController: {
        text: '循环控制器',
        onclick: function () {
            menuTree.addElement('loopController');
        }
    },
    testFragment: {
        text: '测试片段',
        onclick: function () {
            menuTree.addElement('testFragment');
        }
    },
    viewResultTree: {
        text: '查看结果树',
        onclick: function () {
            menuTree.addElement('viewResultTree');
        }
    },
    aggregateReport: {
        text: '聚合报告',
        onclick: function () {
            menuTree.addElement('aggregateReport');
        }
    },
    responseAssertion: {
        text: '响应断言',
        onclick: function () {
            menuTree.addElement('responseAssertion');
        }
    },
    jsonPathAssertion: {
        text: 'JSON断言',
        onclick: function () {
            menuTree.addElement('jsonPathAssertion');
        }
    },
};

let configElements = {
    text: '配置元件',
    children: [
        menuItems.csvDataSet,
        menuItems.httpConfig,
        menuItems.httpHeader,
        menuItems.idGenerator,
        menuItems.dataSourceConfig,
        menuItems.userArgument
    ]
};

let addSampler = {
    text: '取样器',
    children: [
        menuItems.httpSampler,
        menuItems.jdbcRequest,
        menuItems.debugSampler,
        menuItems.beanShellSampler
    ]
};

let beforeProcessor = {
    text: '前置处理器',
    children: [
        menuItems.paramsSign,
        menuItems.jdbcPre,
        menuItems.beanShellPre
    ]
};

let afterProcessor = {
    text: '后置处理器',
    children: [
        menuItems.jsonExtract,
        menuItems.regexExtractor,
        menuItems.aesSecret,
        menuItems.debugPost,
        menuItems.jdbcPost,
        menuItems.beanShellPost,
        menuItems.resultAction
    ]
};

let logicController = {
    text: '逻辑控制器',
    children: [
        menuItems.criticalSection,
        menuItems.ifController,
        menuItems.moduleController,
        menuItems.loopController
    ]
};

let listener = {
    text: '监听器',
    children: [
        menuItems.viewResultTree,
        menuItems.aggregateReport
    ]
};

let assertion = {
    text: '断言',
    children: [
        menuItems.responseAssertion,
        menuItems.jsonPathAssertion
    ]
};

let moveItem = {
    text: '移动菜单位置',
    children: [
        menuItems.moveUp,
        menuItems.moveDown
    ]
};

let baseMenus = [
    menuItems.copy,
    menuItems.paste,
    menuItems.delete,
    menuItems.separator,
    menuItems.enabled,
    menuItems.disabled,
    menuItems.separator,
    menuItems.modifySort,
    moveItem,
    menuItems.separator,
    menuItems.helper
];

let testPlanMenus = [
    menuItems.threadGroup,
    menuItems.separator,
    menuItems.testFragment,
    menuItems.separator,
    menuItems.enabled,
    menuItems.disabled,
    menuItems.separator,
    menuItems.modifySort,
    moveItem,
    menuItems.separator,
    menuItems.paste,
    menuItems.delete,
    menuItems.helper
];

let samplerMenus = [
    configElements,
    menuItems.separator,
    beforeProcessor,
    afterProcessor,
    assertion,
    menuItems.separator,
    logicController,
    listener,
    menuItems.separator,
    menuItems.testFragment,
    menuItems.separator,
    menuItems.enabled,
    menuItems.disabled,
    menuItems.separator,
    menuItems.modifySort,
    moveItem,
    menuItems.separator,
    menuItems.copy,
    menuItems.paste,
    menuItems.delete,
    menuItems.helper
];

let mainMenus = [
    configElements,
    menuItems.separator,
    addSampler,
    menuItems.separator,
    beforeProcessor,
    afterProcessor,
    assertion,
    menuItems.separator,
    logicController,
    listener,
    menuItems.separator,
    menuItems.testFragment,
    menuItems.separator,
    menuItems.enabled,
    menuItems.disabled,
    menuItems.separator,
    menuItems.modifySort,
    moveItem,
    menuItems.separator,
    menuItems.copy,
    menuItems.paste,
    menuItems.delete,
    menuItems.helper
];

let testPlanSet = new Set();
testPlanSet.add('testPlan');

let samplerSet = new Set();
samplerSet.add('httpSampler');
samplerSet.add('jdbcRequest');

let mainMenuSet = new Set();
mainMenuSet.add('threadGroup');
mainMenuSet.add('criticalSection');
mainMenuSet.add('ifController');
mainMenuSet.add('testFragment');
mainMenuSet.add('loopController');
