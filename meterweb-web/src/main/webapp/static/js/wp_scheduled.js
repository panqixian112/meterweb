/**
 * 定时任务相关
 */
function createScheduled() {
    $('#dlg_scheduled').dialog({title:'新增计划任务'}).dialog('open').dialog('center');
    $('#form_scheduled').form('clear');
}

function updateScheduled() {
    let row = $('#dg_scheduled').datagrid('getSelected');
    if(row){
        if(row.scheduledname==='admin'){
            layer.alert('admin用户不可修改！');
            return;
        }
        $('#dlg_scheduled').dialog({title:'修改用户信息'}).dialog('open').dialog('center');
        let scheduledform = $('#form_scheduled');
        scheduledform.form('clear');
        scheduledform.form('load', row);
        $('#form_scheduled_scheduledname').textbox({readonly:true});
        $('#form_scheduled_password').passwordbox({required:false});
        $('#form_scheduled_confirmPassword').passwordbox({required:false});
    }else{
        layer.alert('请选择数据！');
    }
}

function saveScheduled() {
    let form_id =  $('#form_scheduled_id').val();
    let url = contextPath + "/auth/scheduled";
    if(form_id===""){
        url+="/create";
    }else{
        url+="/update";
    }
    let loadLayer;
    $('#form_scheduled').form('submit',{
        url: url,
        onSubmit: function(){
            let isValid = $(this).form('validate');
            if (isValid){
                loadLayer = layer.load(1, {shade: [0.1,'#000']});
            }
            return isValid;
        },
        success: function(result){
            layer.close(loadLayer);
            let data = JSON.parse(result);
            if (data.status === 1){
                layer.alert('保存成功！');
                $('#dg_scheduled').datagrid('reload');
                $('#dlg_scheduled').dialog('close');
            } else {
                layer.alert('保存失败：' + data.errorMsg);
            }
        }
    });
}

function deleteScheduled() {
    let row = $('#dg_scheduled').datagrid('getSelected');
    if(row){
        let ly = layer.confirm('确定要删除吗？', {
            btn: ['确定','取消'] //按钮
        },function () {
            let data = {id:row.id, scheduledId:row.scheduledId};
            $.post(contextPath + '/auth/scheduled/delete', data, function (result) {
                layer.close(ly);
                if(result.status===1){
                    $('#dg_scheduled').datagrid('reload');
                }else{
                    layer.alert(result.errorMsg);
                }
            },'json');
        });
    }else{
        layer.alert('请选择数据！');
    }
}

/**
 * 启动
 */
function startScheduled(){
    let row = $('#dg_scheduled').datagrid('getSelected');
    if (row){
        layerConfirm('你确定要启动此任务吗？',function(r) {
            if (r) {
                let loading = layerLoading(1);
                $.post(contextPath + '/auth/scheduled/start', {id: row.id}, function (result) {
                    layer.close(loading);
                    if (result.status === 1) {
                        setTimeout(() => {
                            $('#dg_scheduled').datagrid('reload');
                        }, 2000);
                    }
                    layer.alert(result.errorMsg);
                }, 'json');
            }
        });
    }else {
        layer.alert('请选择要启动的任务!');
    }
}

/**
 * 停止
 */
function stopScheduled(){
    let row = $('#dg_scheduled').datagrid('getSelected');
    if (row){
        layerConfirm('你确定要停止此任务吗？',function(r){
            if (r){
                let loading = layerLoading(1);
                $.post(contextPath + '/auth/scheduled/stop',{id:row.id},function(result){
                    layer.close(loading);
                    if (result.status === 1) {
                        setTimeout(() => {
                            $('#dg_scheduled').datagrid('reload');
                        }, 2000);
                    }
                    layer.alert(result.errorMsg);
                },'json');
            }
        });
    }else {
        layer.alert('请选择要停止的任务!');
    }
}