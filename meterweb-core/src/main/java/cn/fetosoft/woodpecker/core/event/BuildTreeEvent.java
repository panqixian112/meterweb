package cn.fetosoft.woodpecker.core.event;

import org.springframework.context.ApplicationEvent;

/**
 * 配置结束事件
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 14:04
 */
public class BuildTreeEvent extends ApplicationEvent {

	/**
	 *
	 */
	public BuildTreeEvent(String testId) {
		super(testId);
	}

	public String getTestId(){
		return this.getSource().toString();
	}
}
