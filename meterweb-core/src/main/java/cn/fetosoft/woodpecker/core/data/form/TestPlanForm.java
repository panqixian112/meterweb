package cn.fetosoft.woodpecker.core.data.form;

import cn.fetosoft.woodpecker.core.data.base.BaseForm;
import cn.fetosoft.woodpecker.core.data.base.Condition;
import lombok.Getter;
import lombok.Setter;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.FieldType;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/8 9:57
 */
@Setter
@Getter
@Document("wp_testPlan")
public class TestPlanForm extends BaseForm {

	@Condition(fieldType= FieldType.OBJECT_ID)
	private ObjectId id;

	@Condition
	private String planId;

	@Condition
	private String name;
}
