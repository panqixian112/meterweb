package cn.fetosoft.woodpecker.core.jmeter.element.controller;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.control.TestFragmentController;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建测试片段
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/23 13:41
 */
@Component("testFragmentImpl")
public class CreateTestFragmentImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		return new TestFragmentController();
	}
}
