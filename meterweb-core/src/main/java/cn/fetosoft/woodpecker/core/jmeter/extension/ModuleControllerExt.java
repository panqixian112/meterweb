package cn.fetosoft.woodpecker.core.jmeter.extension;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import org.apache.jmeter.control.GenericController;
import org.apache.jmeter.control.ReplaceableController;
import org.apache.jmeter.gui.tree.JMeterTreeNode;
import org.apache.jorphan.collections.HashTree;
import java.util.List;

/**
 * 自定义模块控制器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/27 9:11
 */
public class ModuleControllerExt extends GenericController implements ReplaceableController {

	private List<BaseElement> children;

	public List<BaseElement> getChildren() {
		return children;
	}

	public void setChildren(List<BaseElement> children) {
		this.children = children;
	}

	@Override
	public HashTree getReplacementSubTree() {
		return null;
	}

	@Override
	public void resolveReplacementSubTree(JMeterTreeNode context) {

	}
}
