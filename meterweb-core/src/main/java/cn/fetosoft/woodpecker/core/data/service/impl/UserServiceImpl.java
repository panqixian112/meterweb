package cn.fetosoft.woodpecker.core.data.service.impl;

import cn.fetosoft.woodpecker.core.data.base.AbstractMongoService;
import cn.fetosoft.woodpecker.core.data.entity.User;
import cn.fetosoft.woodpecker.core.data.form.UserForm;
import cn.fetosoft.woodpecker.core.data.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 用户操作
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/14 21:44
 */
@Slf4j
@Service
public class UserServiceImpl extends AbstractMongoService<User, UserForm> implements UserService {

    @Override
    public User findByUserId(String userId) {
        UserForm form = new UserForm();
        form.setUserId(userId);
        try {
            List<User> list = this.selectListByForm(form, User.class);
            if(!CollectionUtils.isEmpty(list) && list.size()==1){
                return list.get(0);
            }
        } catch (Exception e) {
            log.error("findByUserId", e);
        }
        return null;
    }

    @Override
    public User login(String username, String password) {
        UserForm form = new UserForm();
        form.setUsername(username);
        form.setPassword(password);
        try {
            List<User> list = this.selectListByForm(form, User.class);
            if(!CollectionUtils.isEmpty(list) && list.size()==1){
                return list.get(0);
            }
        } catch (Exception e) {
            log.error("login", e);
        }
        return null;
    }

    @Override
    public User findByUsername(String username) {
        UserForm form = new UserForm();
        form.setUsername(username);
        try {
            List<User> list = this.selectListByForm(form, User.class);
            if(!CollectionUtils.isEmpty(list) && list.size()==1){
                return list.get(0);
            }
        } catch (Exception e) {
            log.error("findByUsername", e);
        }
        return null;
    }
}
