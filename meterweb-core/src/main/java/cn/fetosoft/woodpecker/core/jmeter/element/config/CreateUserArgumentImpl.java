package cn.fetosoft.woodpecker.core.jmeter.element.config;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.config.UserArgumentElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.jmeter.config.Argument;
import org.apache.jmeter.config.Arguments;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建用户定义的变量
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/15 21:52
 */
@Component("userArgumentImpl")
public class CreateUserArgumentImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        UserArgumentElement element = (UserArgumentElement) be;
        Arguments arguments = new Arguments();
		String[] names = element.getNames();
		String[] values = element.getValues();
        if(names!=null && values!=null && names.length==values.length){
			String[] dests = element.getDescriptions();
			for(int i=0; i<names.length; i++){
				Argument argument = new Argument(names[i], values[i], "=", dests[i]);
				arguments.addArgument(argument);
			}
        }
        return arguments;
    }
}
