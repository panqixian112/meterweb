package cn.fetosoft.woodpecker.core.jmeter.extension;

import org.apache.jmeter.config.ConfigElement;
import org.apache.jmeter.protocol.http.control.HeaderManager;
import org.apache.jmeter.protocol.http.sampler.HTTPSamplerBase;
import org.apache.jmeter.testelement.AbstractTestElement;
import org.apache.jmeter.testelement.TestStateListener;
import org.apache.jmeter.threads.JMeterVariables;

import java.io.Serializable;

/**
 * HttpHeader配置
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/11 14:09
 */
public class HttpHeaderConfig extends AbstractTestElement implements ConfigElement, TestStateListener, Serializable {

	private HeaderManager headerManager;

	@Override
	public void addConfigElement(ConfigElement config) {

	}

	@Override
	public boolean expectsModification() {
		return false;
	}

	@Override
	public void testStarted() {
		if(headerManager!=null){
			JMeterVariables vars = this.getThreadContext().getVariables();
			vars.putObject(HTTPSamplerBase.HEADER_MANAGER, this.getHeaderManager());
		}
	}

	@Override
	public void testStarted(String host) {
		this.testStarted();
	}

	@Override
	public void testEnded() {

	}

	@Override
	public void testEnded(String host) {

	}

	public HeaderManager getHeaderManager() {
		return headerManager;
	}

	public void setHeaderManager(HeaderManager headerManager) {
		this.headerManager = headerManager;
	}
}
