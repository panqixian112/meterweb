package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.ParamsSignElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import cn.fetosoft.woodpecker.core.jmeter.processor.ParamsSignProcessor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建参数签名处理器
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/8/8 16:41
 */
@Component("paramsSignImpl")
public class CreateParamsSignProcessorImpl extends AbstractElementBuild {

    @Override
    protected TestElement convert(BaseElement be) throws Exception {
        ParamsSignElement element = (ParamsSignElement) be;
        ParamsSignProcessor processor = new ParamsSignProcessor();
		processor.setSignType(element.getSignType());
		processor.setSignName(element.getSignName());
		processor.setSignKey(element.getSignKey());
		processor.setParamName(element.getParamName());
		processor.setJsonPath(element.getJsonPath());
		processor.setSignJoiner(element.getSignJoiner());
        return processor;
    }
}
