package cn.fetosoft.woodpecker.core.data.entity.element.controller;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * 临界控制器参数
 * @author guobingbing
 * @wechat t_gbinb
 * @since 2021/7/11 20:14
 */
@Setter
@Getter
@Document("wp_element")
public class CriticalSectionElement extends BaseElement {

    @DataField
    private String lockName = "global_lock";
}
