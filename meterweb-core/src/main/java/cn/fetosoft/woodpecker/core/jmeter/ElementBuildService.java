package cn.fetosoft.woodpecker.core.jmeter;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import org.apache.jmeter.testelement.TestElement;

/**
 * 测试组件创建接口
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 9:47
 */
public interface ElementBuildService {

	/**
	 * 创建测试组件
	 * @param element
	 * @return
	 * @throws Exception
	 */
	TestElement build(BaseElement element) throws Exception;
}
