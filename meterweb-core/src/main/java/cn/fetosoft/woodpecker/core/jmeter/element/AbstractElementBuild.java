package cn.fetosoft.woodpecker.core.jmeter.element;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.jmeter.ElementBuildService;
import cn.fetosoft.woodpecker.core.util.Constant;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.testelement.TestElement;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 9:51
 */
public abstract class AbstractElementBuild implements ElementBuildService {

	protected static final String SCOPE_ALL = "all";
	protected static final String SCOPE_PARENT = "parent";
	protected static final String SCOPE_CHILDREN = "children";
	protected static final String SCOPE_VARIABLE = "variable";

	@Override
	public TestElement build(BaseElement element) throws Exception {
		if(StringUtils.isBlank(element.getUserId())){
			throw new Exception("The field named userId is empty!!!");
		}
		if(StringUtils.isBlank(element.getTestId())){
			throw new Exception("The field named testId is empty!!!");
		}
		TestElement testElement = this.convert(element);
		testElement.setName(element.getName());
		testElement.setComment(element.getDest()==null?"":element.getDest());
		testElement.setEnabled(element.getEnabled()==null?true:element.getEnabled());
		testElement.setProperty(TestElement.TEST_CLASS, testElement.getClass().getName());
		testElement.setProperty(Constant.EID, element.getId());
		testElement.setProperty(Constant.PLAN_ID, element.getPlanId());
		testElement.setProperty(Constant.ELEMENT_ID, element.getId());
		testElement.setProperty(Constant.PARENT_ID, element.getParentId());
		testElement.setProperty(Constant.CATEGORY, element.getCategory());
		testElement.setProperty(Constant.USER_ID, element.getUserId());
		testElement.setProperty(Constant.TEST_ID, element.getTestId());
		return testElement;
	}

	/**
	 * 数据转换
	 * @param be
	 * @return
	 * @throws Exception
	 */
	protected abstract TestElement convert(BaseElement be) throws Exception;
}
