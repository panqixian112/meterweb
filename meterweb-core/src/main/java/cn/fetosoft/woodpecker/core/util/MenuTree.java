package cn.fetosoft.woodpecker.core.util;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/10 9:16
 */
@Setter
@Getter
public final class MenuTree {

	private String id;

	private String text;

	private String planId;

	private String parentId;

	private String category;

	private String url;

	private String iconCls;

	private String before;

	private String after;

	private String state;

	private int sort;

	private boolean enabled;

	private List<MenuTree> children = new ArrayList<>();

	public MenuTree(String text){
		this(null, text, null, null, null);
	}

	public MenuTree(String text, String category){
		this(null, text, category, null, null);
	}

	public MenuTree(String id, String text, String category){
		this(id, text, category, null, null);
	}

	public MenuTree(String id, String text, String category, String url, String icon){
		this.id = id;
		this.text = text;
		this.category = category;
		this.url = url;
		this.iconCls = icon;
	}

	public void addChild(MenuTree child){
		this.children.add(child);
	}
}
