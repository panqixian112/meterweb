package cn.fetosoft.woodpecker.core.data.entity.element.sampler;

import cn.fetosoft.woodpecker.core.data.base.DataField;
import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Debug采样器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/8/10 17:44
 */
@Setter
@Getter
@Document("wp_element")
public class DebugSamplerElement extends BaseElement {

	@DataField
	private String displayJMeterVariables;
	@DataField
	private String displayJMeterProperties;
	@DataField
	private String displaySystemProperties;
}
