package cn.fetosoft.woodpecker.core.jmeter.extension;

import cn.fetosoft.woodpecker.core.util.RandomUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.config.ConfigTestElement;
import org.apache.jmeter.engine.event.LoopIterationEvent;
import org.apache.jmeter.engine.event.LoopIterationListener;
import org.apache.jmeter.engine.util.NoConfigMerge;
import org.apache.jmeter.engine.util.NoThreadClone;
import org.apache.jmeter.testelement.ThreadListener;
import org.apache.jmeter.threads.JMeterContextService;
import org.apache.jmeter.threads.JMeterVariables;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 随机ID生成器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/7/22 17:24
 */
public class IdGeneratorConfig extends ConfigTestElement implements LoopIterationListener, NoThreadClone, NoConfigMerge, ThreadListener {

	private static final String DELIMITER = ";";
	private static final String MODE_UUID = "uuid";
	private static final String MODE_DATENUM = "datenum";
	private static final String MODE_NUMBER = "number";
	private static final String MODE_VARNUM = "varnum";
	private static final String MODE_TIMESTAMP = "timestamp";
	private static final String MODE_TIMESTAMP_MS = "timestampMs";

	private String mode;
	private String paramNames;
	private Integer length;
	private String pattern;
	private String prefix;

	@Override
	public void iterationStart(LoopIterationEvent iterEvent) {
		if(StringUtils.isNotBlank(paramNames)){
			JMeterVariables variables = JMeterContextService.getContext().getVariables();
			String[] params = paramNames.split(DELIMITER);
			for(String p : params){
				String val = "";
				if(MODE_UUID.equalsIgnoreCase(getMode())){
					val = RandomUtil.uuid();
				}else if(MODE_DATENUM.equalsIgnoreCase(getMode())){
					val = this.dateFormat();
				}else if(MODE_NUMBER.equalsIgnoreCase(getMode())){
					if(getLength()!=null && getLength()>0) {
						val = RandomUtil.getRandomNumber(getLength());
					}
				}else if(MODE_VARNUM.equalsIgnoreCase(getMode())){
					if(getLength()!=null && getLength()>0) {
						val = RandomUtil.getRandomNumChar(getLength());
					}
				}else if(MODE_TIMESTAMP.equalsIgnoreCase(getMode())){
					val = String.valueOf(System.currentTimeMillis()/1000);
				}else if(MODE_TIMESTAMP_MS.equalsIgnoreCase(getMode())){
					val = String.valueOf(System.currentTimeMillis());
				}
				variables.put(p, getPrefix()!=null?(getPrefix()+val):val);
			}
		}
	}

	/**
	 * 格式化日期
	 * @return
	 */
	private String dateFormat(){
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(getPattern());
		if(getLength()!=null && getLength()>0){
			return LocalDateTime.now().format(formatter) + RandomUtil.getRandomNumber(getLength());
		}else {
			return LocalDateTime.now().format(formatter);
		}
	}

	@Override
	public void threadStarted() {
	}

	@Override
	public void threadFinished() {
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getParamNames() {
		return paramNames;
	}

	public void setParamNames(String paramNames) {
		this.paramNames = paramNames;
	}

	public Integer getLength() {
		return length;
	}

	public void setLength(Integer length) {
		this.length = length;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
