package cn.fetosoft.woodpecker.core.jmeter.element.processor;

import cn.fetosoft.woodpecker.core.data.entity.BaseElement;
import cn.fetosoft.woodpecker.core.data.entity.element.processor.DebugPostElement;
import cn.fetosoft.woodpecker.core.jmeter.element.AbstractElementBuild;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.extractor.DebugPostProcessor;
import org.apache.jmeter.testelement.TestElement;
import org.springframework.stereotype.Component;

/**
 * 创建Debug处理器
 * @author guobingbing
 * @version 1.0
 * @wechat t_gbinb
 * @create 2021/6/30 11:50
 */
@Component("debugPostImpl")
public class CreateDebugPostProcessorImpl extends AbstractElementBuild {

	@Override
	protected TestElement convert(BaseElement be) throws Exception {
		DebugPostElement element = (DebugPostElement) be;
		DebugPostProcessor processor = new DebugPostProcessor();
		processor.setProperty(TestElement.TEST_CLASS, DebugPostProcessor.class.getName());
		if(StringUtils.isNotBlank(element.getDisplayJMeterVariables())) {
			processor.setDisplayJMeterVariables(Boolean.parseBoolean(element.getDisplayJMeterVariables()));
		}
		if(StringUtils.isNotBlank(element.getDisplayJMeterProperties())){
			processor.setDisplayJMeterProperties(Boolean.parseBoolean(element.getDisplayJMeterProperties()));
		}
		if(StringUtils.isNotBlank(element.getDisplaySamplerProperties())){
			processor.setDisplaySamplerProperties(Boolean.parseBoolean(element.getDisplaySamplerProperties()));
		}
		if(StringUtils.isNotBlank(element.getDisplaySystemProperties())){
			processor.setDisplaySystemProperties(Boolean.parseBoolean(element.getDisplaySystemProperties()));
		}
		processor.setProperty("displayJMeterProperties", processor.isDisplayJMeterProperties());
		processor.setProperty("displayJMeterVariables", processor.isDisplayJMeterVariables());
		processor.setProperty("displaySamplerProperties", processor.isDisplaySamplerProperties());
		processor.setProperty("displaySystemProperties", processor.isDisplaySystemProperties());
		return processor;
	}
}
